import { Test17Page } from './app.po';

describe('test17 App', function() {
  let page: Test17Page;

  beforeEach(() => {
    page = new Test17Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
