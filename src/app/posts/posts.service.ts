import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';//function that delays loading of data

@Injectable()
export class PostsService {

//private _url = 'http://jsonplaceholder.typicode.com/posts';
postsObservable;
  getPosts (){
    this.postsObservable = this.af.database.list('/posts/').map(
            posts => {
        posts.map(
          post=> {
            post.postUsers = [];
            for(var p in post.users){
             post.postUsers.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    );
    return this.postsObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)
  }
  
    addpost(post){
    this.postsObservable.push(post);//push is a function on array, adds object in end of array.
}
 updatepost(post){
let postKey = post.$key;
let postData = {title:post.title,body:post.body};
this.af.database.object('/posts/' + postKey).update(postData);
}
  deletepost(post){
let postKey = post.$key;
this.af.database.object('/posts/' + postKey).remove();    
   }
  constructor(private af:AngularFire) { }

}