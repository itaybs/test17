import { Component, OnInit,Output, EventEmitter } from '@angular/core';
import {Post} from '../post/post'; //import user component
import {NgForm} from '@angular/forms';

@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
@Output() userAddedEvent= new EventEmitter<Post>();

post:Post = {title:'', body:''};


onSubmit(form:NgForm){
console.log(form);
this.userAddedEvent.emit(this.post);
this.post= {
  title:'',
  body:''
}
}
  constructor() { }

  ngOnInit() {
  }

}